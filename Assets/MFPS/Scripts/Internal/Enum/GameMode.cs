﻿public enum GameMode
{
    TDM,
    FFA,
    CTF,
    //Bomb,
#if BDGM
    SND,
#endif
#if CP
    CP,
#endif
#if GR
    GR,
#endif
#if LMS
    LSM,
#endif
}