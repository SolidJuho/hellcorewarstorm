﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable; //Replace default Hashtables with Photon hashtables

public class bl_AIMananger : bl_PhotonHelper
{

    [SerializeField] private GameObject Team1AIPrefab;
    [SerializeField] private GameObject Team2AIPrefab;
    [SerializeField, Range(2, 16)] public int NumberOfBots = 5;

    private List<bl_AIShooterAgent> AllBots = new List<bl_AIShooterAgent>();
    private List<Transform> AllBotsTransforms = new List<Transform>();

    private bl_GameManager GameManager;
    [SerializeField] private List<PlayersSlots> Team1PlayersSlots = new List<PlayersSlots>();
    [SerializeField] private List<PlayersSlots> Team2PlayersSlots = new List<PlayersSlots>();

    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        GameManager = FindObjectOfType<bl_GameManager>();
    }

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
        if (PhotonNetwork.isMasterClient)
        {
            bool with = (bool)PhotonNetwork.room.CustomProperties[PropertiesKeys.WithBotsKey];
            SetUpSlots();
            if (with)
            {
                if (GetGameMode == GameMode.FFA)
                {
                    for (int i = 0; i < NumberOfBots; i++)
                    {
                        SpawnBot();
                    }
                }
                else if (GetGameMode == GameMode.TDM)
                {
                    int bots = PhotonNetwork.room.MaxPlayers;
                    int half = bots / 2;
                    for (int i = 0; i < half; i++)
                    {
                        SpawnBot(null, Team.Delta);
                    }
                    for (int i = 0; i < half; i++)
                    {
                        SpawnBot(null, Team.Recon);
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void SetUpSlots()
    {
        if(GetGameMode == GameMode.TDM)
        {
            int ptp = NumberOfBots / 2;
            for (int i = 0; i < ptp; i++)
            {
                PlayersSlots s = new PlayersSlots();
                s.Bot = string.Empty;
                s.Player = string.Empty;
                Team1PlayersSlots.Add(s);
            }
            for (int i = 0; i < ptp; i++)
            {
                PlayersSlots s = new PlayersSlots();
                s.Bot = string.Empty;
                s.Player = string.Empty;
                Team2PlayersSlots.Add(s);
            }        
        }
    }

    public void SpawnBot(bl_AIShooterAgent agent = null, Team _team = Team.None)
    {
        Transform t = GameManager.GetAnSpawnPoint;
        string AiName = Team1AIPrefab.name;
        if (agent != null)
        {
            AiName = (agent.AITeam == Team.Recon) ? Team2AIPrefab.name : Team1AIPrefab.name;
            if(GetGameMode == GameMode.TDM)//if team mode, spawn bots in the respective team spawn points.
            {
                if(agent.AITeam == Team.None) { Debug.LogError("This bot has not team"); }

                if (CheckPlayerSlot(agent, agent.AITeam))
                {
                    t = GameManager.GetAnTeamSpawnPoint(agent.AITeam);
                }
                else
                {
                    return;
                }
            }
        }
        else
        {
            AiName = (_team == Team.Recon) ? Team2AIPrefab.name : Team1AIPrefab.name;
            if (GetGameMode == GameMode.TDM)//if team mode, spawn bots in the respective team spawn points.
            {
                t = GameManager.GetAnTeamSpawnPoint(_team);
            }
        }
        GameObject bot = PhotonNetwork.Instantiate(AiName, t.position, t.rotation, 0);
        bl_AIShooterAgent newAgent = bot.GetComponent<bl_AIShooterAgent>();
        if (agent != null)
        {
            newAgent.AIName = agent.AIName;
            newAgent.AITeam = agent.AITeam;
        }
        else
        {
            newAgent.AIName = "AI " + Random.Range(0, 999);
            newAgent.AITeam = _team;
        }
        AllBots.Add(newAgent);
        AllBotsTransforms.Add(newAgent.AimTarget);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    bool CheckPlayerSlot(bl_AIShooterAgent agent, Team team)
    {
        if(GetGameMode == GameMode.TDM)
        {
            if(team == Team.Recon)
            {
                bool already = Team2PlayersSlots.Exists(x => x.Bot == agent.AIName);
                if (already)
                {
                    return true;
                }
                else
                {
                    if (hasSpaceInTeamForBot(Team.Recon))
                    {
                        int index = Team2PlayersSlots.FindIndex(x => x.Player == string.Empty && x.Bot == string.Empty);
                        Team2PlayersSlots[index].Bot = agent.AIName;
                        return true;
                    }
                    else { return false; }
                }
            }
            else
            {
                bool already = Team1PlayersSlots.Exists(x => x.Bot == agent.AIName);
                if (already)
                {
                    return true;
                }
                else
                {
                    if (hasSpaceInTeamForBot(Team.Delta))
                    {
                        int index = Team1PlayersSlots.FindIndex(x => x.Player == string.Empty && x.Bot == string.Empty);
                        Team1PlayersSlots[index].Bot = agent.AIName;
                        return true;
                    }
                    else { return false; }
                }
            }
        }
        return true;
    }

    private bool hasSpaceInTeam(Team team)
    {
       if(team == Team.Recon)
        {
            return Team2PlayersSlots.Exists(x => x.Player == string.Empty);
        }
        else
        {
            return Team1PlayersSlots.Exists(x => x.Player == string.Empty);
        }
    }

    private bool hasSpaceInTeamForBot(Team team)
    {
        if (team == Team.Recon)
        {
            return Team2PlayersSlots.Exists(x => x.Player == string.Empty && x.Bot == string.Empty);
        }
        else
        {
            return Team1PlayersSlots.Exists(x => x.Player == string.Empty && x.Bot == string.Empty);
        }
    }

    public void OnBotDeath(bl_AIShooterAgent agent, bl_AIShooterAgent killer)
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        AllBots.Remove(agent);
        AllBotsTransforms.Remove(agent.AimTarget);
        for (int i = 0; i < AllBots.Count; i++)
        {
            AllBots[i].CheckTargets();
        }
        SpawnBot(agent);
    }

    public List<Transform> GetOtherBots(Transform bot, Team _team)
    {
        List<Transform> all = new List<Transform>();
        if (GetGameMode == GameMode.FFA)
        {
            all.AddRange(AllBotsTransforms);
            for (int i = 0; i < all.Count; i++)
            {
                if (all[i].name.Contains("die"))
                {
                    all.RemoveAt(i);
                }
            }
        }
        else //if TDM game mode
        {
            all.AddRange(AllBotsTransforms);
            for (int i = 0; i < all.Count; i++)
            {
                if (all[i].name.Contains("die") || all[i].root.GetComponent<bl_AIShooterAgent>().AITeam == _team)
                {
                    all.RemoveAt(i);
                }
            }
        }
        all.Remove(bot);
        return all;
    }

    public bl_AIShooterAgent GetBot(int viewID)
    {
        foreach(bl_AIShooterAgent agent in AllBots)
        {
            if(agent.photonView.viewID == viewID)
            {
                return agent;
            }
        }
        return null;
    }

    public override void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        if (GetGameMode != GameMode.TDM)
            return;

        Hashtable hat = playerAndUpdatedProps[1] as Hashtable;
        PhotonPlayer player = playerAndUpdatedProps[0] as PhotonPlayer;
        if (hat.ContainsKey(PropertiesKeys.TeamKey))
        {
            if((string)hat[PropertiesKeys.TeamKey]  == Team.Recon.ToString())
            {
                if (Team2PlayersSlots.Exists(x => x.Player == player.NickName)) return;
                int index = Team2PlayersSlots.FindIndex(x => x.Player == string.Empty);
                if (index != -1)
                {
                    Team2PlayersSlots[index].Player = player.NickName;
                    Team2PlayersSlots[index].Bot = string.Empty;
                }
            }
            else
            {
                if (Team1PlayersSlots.Exists(x => x.Player == player.NickName)) return;
                int index = Team1PlayersSlots.FindIndex(x => x.Player == string.Empty);
                if (index != -1)
                {
                    Team1PlayersSlots[index].Player = player.NickName;
                    Team1PlayersSlots[index].Bot = string.Empty;
                }
            }
        }
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
      
    }

    [System.Serializable]
    public class PlayersSlots
    {
        public string Player;
        public string Bot;
    }
}