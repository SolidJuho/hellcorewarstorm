﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ProgressTrackerUI : MonoBehaviour
{
    [SerializeField]GameObject xpSlider;
    [SerializeField] Image progressBar;
    [SerializeField] TextMeshProUGUI curLvl;
    [SerializeField] TextMeshProUGUI nextLvl;
    [SerializeField] TextMeshProUGUI curXP;
    [SerializeField] TextMeshProUGUI requiredXP;

    PlayerXPSystem XPsys;

    // Start is called before the first frame update
    void Start()
    {
        XPsys = PlayerXPSystem.instance;
        DatabaseManager.playerLoadedInfo += SetupTrack;

    }

    private void OnDisable()
    {
        DatabaseManager.playerLoadedInfo -= SetupTrack;

    }

    /*
    private void Update()
    {
        SetupTrack();
    }*/


    private void SetupTrack()
    {
        curLvl.text = XPsys.GetPlayerLevel().ToString();
        curXP.text = XPsys.GetPlayerXP().ToString();
        nextLvl.text = (XPsys.GetPlayerLevel() + 1).ToString();
        requiredXP.text = XPsys.GetPlayerRequiredXP().ToString();


        progressBar.fillAmount = XPsys.nextLevelProgress();

    }

}
