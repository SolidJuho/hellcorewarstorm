﻿using UnityEngine;
using TMPro;

public class UpdatePlayerInfo : MonoBehaviour
{
    PlayerCreditSystem PCsystem;
    [SerializeField] TextMeshProUGUI PlayerLevel;
    [SerializeField] TextMeshProUGUI PlayerCredits;
    [Header("Debug info")]
    [SerializeField] int credits;
    private void Start()
    {
        PCsystem = PlayerCreditSystem.instance;
        DatabaseManager.playerLoadedInfo += updateInfo;
        
    }

    private void OnDisable()
    {
        DatabaseManager.playerLoadedInfo -= updateInfo;

    }

    void updateInfo()
    {
        Debug.Log("Updating Player Info");
        PlayerCredits.text = PCsystem.getCredits().ToString();
        credits = PCsystem.getCredits();

    }
}
