﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class DatabaseManager : MonoBehaviour
{

    public static DatabaseManager instance;

    public string[] loadeduserdata;

    #region Delegates

    public delegate void PlayerLoadedInfo();
    public static PlayerLoadedInfo playerLoadedInfo;

    #endregion

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            return;
        }

        /*string project = "hellcorewarstorm-e9d91";
        FirestoreDb db = FirestoreDb.Create(project);
        Debug.Log("Created Cloud Firestore client with project ID: " + project);
        */


    }



    #region Database processing
    //Basicly test login.
    IEnumerator auth(string username, string authID)
    {
        WWWForm form = new WWWForm();
        form.AddField("loginUser", username);
        form.AddField("loginID", authID);

        using (UnityWebRequest www = UnityWebRequest.Post("http://ec2-18-220-76-81.us-east-2.compute.amazonaws.com/authUser.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }

    IEnumerator loadBasicInfo(string username, string authID)
    {
        Debug.Log("Loading basic info from DB");
        WWWForm form = new WWWForm();
        form.AddField("loginUser", username);
        form.AddField("loginID", authID);

        using (UnityWebRequest www = UnityWebRequest.Post("http://ec2-18-220-76-81.us-east-2.compute.amazonaws.com/loadUserInformation.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log( www.downloadHandler.text.TrimStart());
                loadeduserdata = www.downloadHandler.text.Split(',');

                if (loadeduserdata != null)
                {
                    PlayerXPSystem xpSys = GetComponent<PlayerXPSystem>();


                    int curlvl = int.Parse(loadeduserdata[1]);
                    int curxp = int.Parse(loadeduserdata[2]);
                    xpSys.setStats(curxp, curlvl);

                    PlayerCreditSystem.instance.setBalance(int.Parse(loadeduserdata[3]));

                    playerLoadedInfo();
                }

                //Make loadbasicinfo with explanations and without.
                //without for real process data and explanations for debug.
            }
        }
    }

    /// <summary>
    /// Only supports saving XP and levels. Also, process this only at end of the match.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="authID"></param>
    /// <returns></returns>
    IEnumerator savePlayerXP(string username, string authID, int curXP, int curLVL)
    {
        Debug.Log("Loading basic info from DB");
        WWWForm form = new WWWForm();
        form.AddField("loginUser", username);
        form.AddField("loginID", authID);
        form.AddField("curXP", curXP);
        form.AddField("curLVL", curLVL);

        using (UnityWebRequest www = UnityWebRequest.Post("http://ec2-18-220-76-81.us-east-2.compute.amazonaws.com/updatePlayerXP.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text.TrimStart());
                PlayerXPSystem.instance.isXPqueued = false;
                //Checks balance
                Invoke("loadDatabaseData",3f);
            }
        }
    }
    #endregion


    public void saveDatabaseData()
    {
        //update xp
        PlayerXPSystem xpSys = GetComponent<PlayerXPSystem>();
        StartCoroutine(savePlayerXP(LoginAuthSystem.instance.getUsername, LoginAuthSystem.instance.getUserID, xpSys.GetPlayerXP(), xpSys.GetPlayerLevel()));


    }

    public void loadDatabaseData()
    {
        //StartCoroutine(auth(LoginAuthSystem.instance.getUsername, LoginAuthSystem.instance.getUserID));


        StartCoroutine(loadBasicInfo(LoginAuthSystem.instance.getUsername, LoginAuthSystem.instance.getUserID));


        //Invoke("loadDatabaseData", 4f); //TODO Lower this for release.
        /*Userdata data = SaveGame.Load<Userdata>(LoginAuthSystem.instance.getUserID);
        
        //update xp
*/

        
    }



}
