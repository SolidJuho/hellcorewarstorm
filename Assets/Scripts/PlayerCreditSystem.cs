﻿using UnityEngine;

public class PlayerCreditSystem : MonoBehaviour
{
    public static PlayerCreditSystem instance;

    private void Awake()
    {
        if(instance == null)
            instance = this;
    }

    int credits;

    public int getCredits()
    {
        return credits;
    }

    //TODO Move all of this to server side!
    public bool purchase(int _price)
    {
        DatabaseManager.instance.loadDatabaseData();

        if (_price > credits)
        {
            credits -= _price;
            return true;
        }
        else
        {
            return false;

        }

    }

    public void addBalance(int _amount)
    {
        credits += _amount;
        //DatabaseManager.instance.saveDatabaseData();

    }

    public void setBalance(int _balance)
    {
        credits = _balance;
    }
}
