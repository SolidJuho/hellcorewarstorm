﻿using UnityEngine;

public class PlayerXPSystem : MonoBehaviour
{
    public static PlayerXPSystem instance;

    [SerializeField]
    int curPlayerXP;

    [SerializeField]
    int curPlayerLVL;

    [SerializeField]
    int requiredXP;

    [SerializeField]
    AnimationCurve xpCurve;

    [SerializeField] float nextLevelProgression;

    [HideInInspector] public bool isXPqueued;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogWarning("PlayerXPsystem duplicate detected, Destroying this.");
            Destroy(gameObject);
        }


    }

    public void processKill(bool wasHeadshot)
    {
        if (wasHeadshot)
        {
            AddPlayerXP(8);

        }
        else
        {
            AddPlayerXP(5);

        }
    }

    void AddPlayerXP(int _amount)
    {
        //Any XP Boosts?

        requiredXP = Mathf.RoundToInt(xpCurve.Evaluate(curPlayerLVL));

        curPlayerXP += _amount;
        if(curPlayerXP > requiredXP)
        {
            curPlayerLVL++;
            Debug.Log("Player earns level: " + curPlayerLVL.ToString());
            curPlayerXP = curPlayerXP - requiredXP;
            requiredXP = Mathf.RoundToInt(xpCurve.Evaluate(curPlayerLVL));

        }
        if (!isXPqueued)
        {
            isXPqueued = true;
        }
    }

    public int GetPlayerLevel()
    {
        return curPlayerLVL;
    }
    public int GetPlayerXP()
    {
        return curPlayerXP;
    }

    public int GetPlayerRequiredXP()
    {
        return Mathf.RoundToInt(xpCurve.Evaluate(curPlayerLVL));
    }

    public void setStats(int _curXp, int _curLvl)
    {
        curPlayerLVL = _curLvl;
        curPlayerXP = _curXp;
    }

    public float nextLevelProgress()
    {
        requiredXP = Mathf.RoundToInt(xpCurve.Evaluate(curPlayerLVL));
        nextLevelProgression = (float)curPlayerXP / requiredXP;
        return nextLevelProgression;
    }
}
