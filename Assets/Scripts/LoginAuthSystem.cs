﻿using UnityEngine;
using Firebase.Auth;
using TMPro;
using UnityEngine.Networking;
using System.Collections;

public class LoginAuthSystem : MonoBehaviour
{

    [Header("Login stuff")]
    [SerializeField] string displayName;
    [SerializeField] string emailAddress;
    [SerializeField] string password;
    [SerializeField] TMP_InputField usernameInput;
    [SerializeField] TMP_InputField emailInput;
    [SerializeField] TMP_InputField passwordInput;
    [SerializeField] TMP_InputField passwordInputAgain;
    [SerializeField] TMP_InputField emailPasswordReset;
    [SerializeField] GameObject verificationReminder;
    [SerializeField] GameObject registerUsername;
    [SerializeField] GameObject Lobby;
    [SerializeField] GameObject AuthUI;
    [SerializeField] GameObject AuthStuff;
    [SerializeField] GameObject registerInfo;
    [SerializeField] GameObject notmatcherror;
    [SerializeField] GameObject tryagainerror;
    [SerializeField] GameObject ForgotPasswordPanel;
    FirebaseAuth auth;
    FirebaseUser user;

    public bool PlayerValidated = false;

    #region Singleton/Awake
    public static LoginAuthSystem instance;

    private void Awake()
    {
        Debug.Log("Database awakes");
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        InitializeFirebase();

        if (PlayerValidated)
        {
            return;
        }
        AuthUI.SetActive(true);
        Lobby.SetActive(false);
    }
    #endregion

    #region Login and Authentication


    public void PressLogin()
    {
        emailAddress = emailInput.text.Trim();
        password = passwordInput.text.Trim();
        login(emailAddress, password);
    }

    public void PressRegister()
    {
        emailAddress = emailInput.text.Trim();
        password = passwordInput.text.Trim();
        if(password == passwordInputAgain.text.Trim() && password != "")
        {
            signup(emailAddress, password);

        }
        else
        {
            Debug.LogError("Passwords dont match");
            registerInfo.SetActive(false);
            notmatcherror.SetActive(true);
        }
        
    }

    public void displayNameReset()
    {
        
        registerUsername.SetActive(true);
        Debug.Log("registerUsername =" + registerUsername.activeSelf);
        notmatcherror.SetActive(false);
    }

    public void PressRegisterDisplayName()
    {
        if (usernameInput.text == "") return;
        FirebaseUser user = auth.CurrentUser;
        if (user != null)
        {
            UserProfile profile = new UserProfile
            {
                DisplayName = usernameInput.text,
            };
            user.UpdateUserProfileAsync(profile).ContinueWith(task => {
                if (task.IsCanceled)
                {
                    Debug.LogError("UpdateUserProfileAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("UpdateUserProfileAsync encountered an error: " + task.Exception);
                    return;
                }

                Debug.Log("User profile updated successfully.");


            });
            registerUsername.SetActive(false);
        }



    }

    public void HideVerification()
    {
        verificationReminder.SetActive(false);

    }

    void verificationTest()
    {
        if (user.IsEmailVerified)
        {
            if (user.DisplayName == "")
            {
                displayNameReset();
                return;
            }
            //User is all ok to enter the game

            StartCoroutine(registerauth());

            PhotonNetwork.player.NickName = displayName;
            //bl_GameData.Instance.setCorrectInformation();
            Lobby.SetActive(true);
            AuthUI.SetActive(false);

            PlayerValidated = true;

            //load data from database
            DatabaseManager.instance.loadDatabaseData();
            RoleCheck();

        }
        else
        {
            Debug.LogError("Email not verified");
            verificationReminder.SetActive(true);
        }
    }

    public void RoleCheck()
    {
        StartCoroutine(roleCheckFromDB(user.DisplayName, user.UserId, user.Email));
    }





    #region firebase Internal
    void InitializeFirebase()
    {
        auth = FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
                verificationTest();
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
                displayName = user.DisplayName ?? "";
                emailAddress = user.Email ?? "";
                verificationTest();
                

            }
        }

    }

    private void login(string email, string password)
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                tryagainerror.SetActive(true);

                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
        verificationTest();

    }

    private void signup(string email, string password)
    {
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                tryagainerror.SetActive(true);

                return;
            }

            // Firebase user has been created.
            FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);

            newUser.SendEmailVerificationAsync();
            


        });




    }


    #endregion

    IEnumerator registerauth()
    {
        WWWForm form = new WWWForm();
        form.AddField("loginUser", user.DisplayName);
        form.AddField("loginID", user.UserId);
        form.AddField("loginEmail", user.Email);

        using (UnityWebRequest www = UnityWebRequest.Post("http://ec2-18-220-76-81.us-east-2.compute.amazonaws.com/register.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }


    IEnumerator roleCheckFromDB(string username, string authID, string email)
    {
        yield return new WaitForSecondsRealtime(3f);
        WWWForm form = new WWWForm();
        form.AddField("loginUser", username);
        form.AddField("loginID", authID);

        using (UnityWebRequest www = UnityWebRequest.Post("http://ec2-18-220-76-81.us-east-2.compute.amazonaws.com/rolecheck.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                string[] loadeduserdata = www.downloadHandler.text.Split(',');

                if (loadeduserdata[1] != null)
                {
                    Debug.Log("Role is: " + loadeduserdata[1]);
                    bl_GameData.Instance.setCorrectInformation(int.Parse(loadeduserdata[1]));


                }

                //Make loadbasicinfo with explanations and without.
                //without for real process data and explanations for debug.
            }
        }
    }

    #endregion


    #region Password Reset
    public void openResetPasswordField()
    {
        ForgotPasswordPanel.SetActive(true);
        AuthStuff.SetActive(false);
    }

    public void cancelResetPassword()
    {
        AuthStuff.SetActive(true);
        ForgotPasswordPanel.SetActive(false);
    }

    public void PressResetPassword()
    {
        auth.SendPasswordResetEmailAsync(emailPasswordReset.text).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                //Hide reset email field.
                AuthStuff.SetActive(true);
                ForgotPasswordPanel.SetActive(false);
                tryagainerror.SetActive(true);
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                //Hide reset email field.
                AuthStuff.SetActive(true);
                tryagainerror.SetActive(true);
                ForgotPasswordPanel.SetActive(false);
                return;


            }

            Debug.Log("Password reset email sent successfully.");
                tryagainerror.SetActive(false);
        });

        //Hide reset email field.
        AuthStuff.SetActive(true);
        ForgotPasswordPanel.SetActive(false);

    }

    #endregion

    public string getUsername
    {
        get
        {
            return user.DisplayName;
        }
    }
    public string getUserID
    {
        get
        {
            return user.UserId;
        }
    }

}




//Class
/*
[System.Serializable]
public class user
{
    public string name;
    public int age;
}

public class program
{
    user user1;
    user user2;
    
    public void setData()
    {
        user1.name = "Kappa";
        user2.name = "Keepo";
        user1.age = 11;
        user2.age = 11;
        SameAge(user1, user2);
    }

    public void SameAge(user _u1, user _u2)
    {
        if(_u1.age == _u2.age)
        {
            Debug.Log("User1 and User2 age is same, they'r names are: " + _u1.name + " and " + _u2.name);
        }
        else
        {
            Debug.Log("Users are not same aged");
        }
    }
}*/