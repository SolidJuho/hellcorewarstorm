﻿using UnityEngine;

namespace Hellcore.Progress
{
    [System.Serializable]
    public class ClassSkill
    {
        public int SkillID;
        public string Name;
        [TextArea]
        public string Description;
        public int Tier;

        public int ParentID;

    }

    [System.Serializable]
    public class PlayerClass
    {
        public string Name;
        public ClassSkill[] Skills;
    }

    public enum PlayerClasses
    {
        Assault = 1,
        Support = 2,
        Veteran = 3,
        Recon = 4,
        Marksman = 5
    }
}
