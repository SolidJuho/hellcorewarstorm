﻿using UnityEngine;

public class CustomGameRoleCheck : MonoBehaviour
{
    [SerializeField]bool isThisHidden;
    private void Start()
    {
        bl_GameData.Role pRole = bl_GameData.Instance.CurrentTeamUser.m_Role;
        if (pRole == bl_GameData.Role.Developer|| pRole == bl_GameData.Role.Moderator)
        {
            //Player has correct access level.
            isThisHidden = false;
            return;
        }
        isThisHidden = true;
        gameObject.SetActive(false);
    }
}
